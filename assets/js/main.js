function previewImage(event){
  const reader = new FileReader()
  const fieldImage = document.getElementById('imgPreview')
  reader.onload = () => {
    if(reader.readyState == 2){
      fieldImage.src = reader.result
    }
  }
  reader.readAsDataURL(event.target.files[0])
  //console.log(reader)
}

$(document).ready(function(){

  $('#price').on('blur', function(){
    $(this).mask('000.000.000.000.000,00', {reverse: true})
  })

  $('.danger.btn.delete').on('click', function(e){
    //e.preventDefault()
    const id = $(this).attr('data-id')
    const categoria = $(this).attr('data-name')
    const msg = `Confirma a exclusão do item: \n${categoria}`
    console.log(id);
    const confirmado = confirm(msg)
    if (confirmado) {
      window.location.href = `?module=dashboard&action=category_delete&id=${id}`
    } else {
      return false
    }
  })

  $('#exportToCSV').on('click', function(){
    const url = $(this).attr('data-url');
    const table = $(this).attr('data-table');

    window.location.href = `${url}&table=${table}`
    return;
  }) 
    
})
