CREATE TABLE IF NOT EXISTS `loja`.`produtos`(
  `id` VARCHAR(150) NOT NULL,
  `produto` VARCHAR(100) NOT NULL,
  `sku` VARCHAR(100) NOT NULL,
  `preco` DECIMAL(13,2) NOT NULL,
  `descricao` mediumtext,
  `quantidade` INT NOT NULL,
  PRIMARY KEY (`id`)
  )ENGINE = InnoDB DEFAULT CHARSET=UTF8MB4;

CREATE TABLE IF NOT EXISTS `loja`.`categorias`(
  `id` VARCHAR(150) NOT NULL,
  `categoria` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`) 
)ENGINE = InnoDB DEFAULT CHARSET=UTF8MB4;
  
INSERT INTO `loja`.`categorias` 
VALUES ('d12der542', 'Eletrodoméstico'),
 ('lskf8787', 'Moveis');

CREATE TABLE IF NOT EXISTS `loja`.`produtos_categorias`(
  `id` VARCHAR(150) NOT NULL,
  `produto_id` VARCHAR(150) NOT NULL,
  `categoria_id` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `idx_produtos_categorias_produto_id` (`produto_id`),
  INDEX `idx_produtos_categorias_categoria_id` (`categoria_id`),
  CONSTRAINT `fk_produtos_categorias_produto_id` 
    FOREIGN KEY (`produto_id`)  
    REFERENCES `loja`.`produtos` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_produtos_categorias_categoria_id`
    FOREIGN KEY (`categoria_id`)
    REFERENCES `loja`.`categorias` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION
)ENGINE = InnoDB DEFAULT CHARSET=UTF8MB4;

ALTER TABLE `loja`.`produtos` 
CHANGE COLUMN `sku` `codigo` VARCHAR(100) NOT NULL ;

ALTER TABLE `loja`.`produtos` 
ADD COLUMN `imagem` VARCHAR(150) NULL AFTER `quantidade`;

ALTER TABLE `loja`.`produtos` 
ADD COLUMN `created_at` DATETIME NULL DEFAULT NOW() AFTER `imagem`;

ALTER TABLE `loja`.`produtos` 
ADD COLUMN `updated_at` DATETIME NULL AFTER `create_at`;

ALTER TABLE `loja`.`categorias` 
ADD COLUMN `codigo` VARCHAR(100) NULL AFTER `id`;

ALTER TABLE `loja`.`categorias` 
ADD COLUMN `created_at` DATETIME NULL DEFAULT NOW() AFTER `categoria`;

ALTER TABLE `loja`.`categorias` 
ADD COLUMN `updated_at` DATETIME NULL AFTER `create_at`;

ALTER TABLE `loja`.`produtos_categorias` 
ADD COLUMN `create_at` DATETIME NULL DEFAULT NOW() AFTER `categoria_id`;

ALTER TABLE `loja`.`produtos_categorias` 
ADD COLUMN `updated_at` DATETIME NULL AFTER `create_at`;

INSERT INTO `loja`.`categorias` VALUES ('5601d91b8b6d54b182e1a2946445d284','214546','Eletrodomésticos','2019-05-18 17:13:08'),('89a9b2b9c4f9fed24b391656d0142b03','458965','Eletrônicos','2019-05-18 17:10:44'),('8b70f3db5dd6b53375a1cccf9a7ceab8','5d2f2d5','Móveis','2019-05-18 17:17:18');
INSERT INTO `loja`.`produtos` VALUES ('3b1d4870c18a25b1b63fe3316d15dc8f','Painel Bancada para TV até 48\" Navi Branco Tx - Móveis Bechara','3597458',112.23,'Com designer simples, tra&ccedil;os leves e propor&ccedil;&otilde;es compactas o Painel Bancada para TV at&eacute; 48\" Navi Branco Tx &eacute; a escolha perfeita para completar a decora&ccedil;&atilde;o da sua casa. Pensado para compor espa&ccedil;os pr&aacute;ticos e funcionais, desde salas de estar, quartos e home cinema. Ele &eacute; produzido com mat&eacute;ria prima de excelente qualidade, possui &oacute;timo acabamento, e conta com nichos multiuso para que voc&ecirc; possa colocar aparelhos eletr&ocirc;nicos e enfeites.\r\n\r\nImagens Meramente Ilustrativas.\r\nTodas as Informa&ccedil;&otilde;es divulgadas s&atilde;o de responsabilidade do Fabricante/Fornecedor.\r\nN&atilde;o nos responsabilizamos pela montagem/instala&ccedil;&atilde;o dos produtos.',11,'22b614f21d640c05934c9f55a48f2d8e0cd514bf/1558241474455849493.jpg','2019-05-19 01:51:14'),('7af9992f2a157f3b2720218cb3b0138c','Guarda Roupa Carraro 1095 Argos 3 Portas de Correr 5 Gavetas - - Cor Native ou Native c/ Atacama','2874875',2272.89,'Medidas: Altura: 218 cm Largura: 240 cm Profundidade: 47 cm \r\n\r\nO Guarda Roupa Casal R5569| 8 Portas 4 Gavetas M&oacute;veis Arapongas &eacute; o roupeiro com qualidade para seu ambiente.Fabricado em Mdp com pintura Uv, o que deixa seu roupeiro resistente e dur&aacute;vel. Possui puxadores em alum&iacute;nio, espa&ccedil;o interno ideal para suas roupas e pertences, com as divis&otilde;es internas necess&aacute;rias para acomodar todas suas coisas.\r\n\r\nGarantia: 90 Dias Contra defeitos de fabrica&ccedil;&atilde;o.',10,'35cf95bbae54d820ab8b5f5601fec2b96fccdadb/15582413431650971337.jpg','2019-05-19 01:49:03'),('861e7bb2f8e5957768b0f1d889d44877','Smartphone Samsung Galaxy S9 Dual Chip Android 8.0 Tela 5.8','9548556',2699.00,'Abertura Dupla\r\nA lente de abertura dupla se adapta como o olho humano, se alternando automaticamente entre v&aacute;rias condi&ccedil;&otilde;es de ilumina&ccedil;&atilde;o com facilidade &ndash; fazendo com que suas fotos fiquem incr&iacute;veis, seja no claro ou com pouca luz, de dia ou de noite. A vers&atilde;o do Galaxy S9+ ainda conta com c&acirc;mera dupla traseira (duas lentes).\r\n\r\n* Abertura dupla com suporte para os modos f 1.5 e f 2.4. Instalado na c&acirc;mera traseira (S9)/c&acirc;mera traseira dupla (S9+).',78,'08fc0f38d003feb364bd1a89bbb49de551c861ea/155823921189548670.png','2019-05-19 01:13:31'),('a82971b97085a693e3ae5e190e1ca830','Smart TV LED 49\" Samsung Ultra HD 4k 49NU7100 com Conversor Digital 3 HDMI 2 USB Wi-Fi Solução Intel','2527895',2098.00,'',45,'fc93521daff140d96dbb5b96304df8e129b24c43/1558241618658802517.png','2019-05-19 01:53:38'),('c2e0a1f97dce18f5e01140308da64dcd','Fogão De Piso Brastemp 4 Bocas BFO4N Inox - Bivolt','099661',769.99,'C&oacute;digo	120277055\r\nC&oacute;digo de barras	7891129233621\r\nFabricante	Brastemp',15,'f3afefa5f38d8b5b5280a2fb7c3af8b39786fb8c/15582412191802741403.jpg','2019-05-19 01:46:59'),('c5847d22dd04100de243bdaaeeb2a613','Micro-ondas Philco PME25 25 Litros com Tecla Preparo Rápido Prata Espelhado','522789',396.90,'Obtenha mais praticidade ao cozinhar com este forno micro-ondas PME25, desse jeito &eacute; poss&iacute;vel preparar deliciosas refei&ccedil;&otilde;es para voc&ecirc; e toda fam&iacute;lia rapidamente. S&atilde;o diversas as fun&ccedil;&otilde;es que v&atilde;o torna o seu dia a dia na cozinha mais f&aacute;cil. Tais como op&ccedil;&otilde;es de cozinhar e descongelar alimentos por peso, fun&ccedil;&atilde;o reaquecer e teclas f&aacute;ceis que elabora os pratos favoritos da garotada.\r\n\r\nFun&ccedil;&atilde;o Descongelar: possui duas op&ccedil;&otilde;es de descongelamento o r&aacute;pido e por peso.\r\n\r\nFun&ccedil;&atilde;o Timer: programe com anteced&ecirc;ncia o hor&aacute;rio do preparo do alimento a ser cozido.\r\n\r\nTeclas F&aacute;ceis: contam com as fun&ccedil;&otilde;es KIDS, que prepara de modo r&aacute;pido receitas como brigadeiro e pipoca. E Cozinhar que aquece e reaquece batatas, lasanha e pizza.\r\n\r\nTrava de Seguran&ccedil;a: ideal para manter o teclado bloqueado, isso evita que pessoas n&atilde;o autorizadas ativem alguma fun&ccedil;&atilde;o. Para bloquear o produto, basta pressionar a tecla cancelar ',10,'fbc8b0607892c0ea28327f36b518c6e2b366c796/1558241081774856622.png','2019-05-19 01:44:41'),('cb6d490410c2c906e47aa9f40e0726c7','Kit Travel Shine Bivolt Secador + Prancha - Philco','13198999',90.00,'informa&ccedil;&otilde;es do produto\r\nFique linda com cabelos mais leves, lisos e brilhosos!\r\n\r\nSecador de Cabelo\r\n\r\n- Leve e compacto\r\n- 2 N&iacute;veis de pot&ecirc;ncia\r\n- Bocal concentrador\r\n- Grade traseira remov&iacute;vel\r\n- Anel para pendurar\r\n- Cord&atilde;o com 1,9m\r\n\r\nPrancha Alisadora\r\n\r\n- Cord&atilde;o girat&oacute;rio com 1,8 m\r\n- Trava para fechamento das placas\r\n- Placas de aquecimento com revestimento, que facilita o uso. \r\n\r\nImagens meramente ilustrativas.\r\nTodas as informa&ccedil;&otilde;es divulgadas s&atilde;o de responsabilidade do Fabricante/Fornecedor.\r\nVerifique com os fabricantes do produto e de seus componentes eventuais limita&ccedil;&otilde;es &agrave; utiliza&ccedil;&atilde;o de todos os recursos e funcionalidades. \r\nConfira a voltagem selecionada antes de finalizar a compra.\r\n',10,'689766e8ab351185eb96382c1811e874fa4fee8a/15582390011891492615.jpg','2019-05-18 18:31:57');
INSERT INTO `loja`.`produtos_categorias` VALUES ('018eb36ac838574e77e35886277dbbd8','a82971b97085a693e3ae5e190e1ca830','5601d91b8b6d54b182e1a2946445d284','2019-05-19 01:53:38'),('33a1b6d35d785249f8c31c132ca47749','cb6d490410c2c906e47aa9f40e0726c7','89a9b2b9c4f9fed24b391656d0142b03','2019-05-19 01:41:03'),('5f052a80c20705807338f9d6921c6726','cb6d490410c2c906e47aa9f40e0726c7','5601d91b8b6d54b182e1a2946445d284','2019-05-19 01:41:03'),('8d62e1301d1c9cf713eda999dcaffbee','c2e0a1f97dce18f5e01140308da64dcd','5601d91b8b6d54b182e1a2946445d284','2019-05-19 01:46:59'),('bb013e7f71634ea6c7d1ff7e86c0e58c','3b1d4870c18a25b1b63fe3316d15dc8f','8b70f3db5dd6b53375a1cccf9a7ceab8','2019-05-19 01:51:14'),('cb4ebe095e0a4d9bc735aa1c9c53da5c','c5847d22dd04100de243bdaaeeb2a613','5601d91b8b6d54b182e1a2946445d284','2019-05-19 01:44:41'),('ccff925bde0b6c81ebc884e3c7478821','a82971b97085a693e3ae5e190e1ca830','89a9b2b9c4f9fed24b391656d0142b03','2019-05-19 01:53:38'),('d8228c95bcde71481bb71d1105193c6d','7af9992f2a157f3b2720218cb3b0138c','8b70f3db5dd6b53375a1cccf9a7ceab8','2019-05-19 01:49:03'),('f9c598a566b5ca0f9679ce62864e6685','861e7bb2f8e5957768b0f1d889d44877','89a9b2b9c4f9fed24b391656d0142b03','2019-05-19 01:38:22');