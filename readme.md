# Requisitos
Ter instalado no ambiente que vai rodar o projeto:
- PHP 7
- Composer
- MySQL

# Dependências
- league/csv

# Instalação das dependências
Dentro da pasta do projeto executar:
- composer install

# Arquivo de configuração
Na raiz do projeto se encontra um arquivo config.php responsável por ter as configurações do ambiente, e lá que se as configurações do banco de dados.

# Criar a pasta uploads e dar permissões para o Apache
Dentro da pasta assets criar a pasta uploads e alterar o dono para seu usuário:apache o usuário
apache pode variar dependendo do sistema onde o apache está sendo executado em sistemas linux
é www-data e em macosx _www

```
sudo chown -R seuusuario:www-data uploads/
```

# Dar permissões total para o usuário Apache na pasta uploads e de execução para os outros
```
sudo chmod 775 -R uploads
```
# Conferindo as permissões que deve ficar idêntico:
```
ls -l
drwxrwxr-x   2 esteves  www-data       64 16 Mai 21:15 uploads
```
# Exportar dados já cadastrado na base MySql
Exportar a partir do arquivo loja.sql

# Acessar a url para executar o script index.php
assessment-backend/