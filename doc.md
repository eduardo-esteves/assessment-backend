# Tecnologias usadas para construir o projeto e suas vantagens:

- PHP 7.1 devido ser Open Source e uma das últimas versões do PHP sanando bugs e vulnerabilidades de segurança. Além de ter uma alta performance de execução comparada as outras versões do PHP.

- Mysql por ser Open Source, ter um ótima performance em consultas SQL além da quantidade de outras ferramentas para se trabalhar em conjunto como por exemplo MysqlWorkBench.

- Padrão MVC, modelo altamente recomendado para projetos de grande porte devido sua alta capacidade de expansão e facilidade na manutenção. Em bora não tive tempo abil para construir todas as telas e funcionalidades que gostaria, me esforcei para entregar um projeto com uma estrutura bem organizada e de fácil manutenção.

- Composer, além de fazer o auto load das classes de forma automatica sem precisar ficar poluindo os códigos com um monte de require() facilita muito quando precisamos instalar bibliotecas de terceiros com apenas um comando via CLI.

- Paradigma de orientação a objetos (oop) programar de forma estruturada acaba dificultando na manutenção devido ser dificil de debugar valores, ter um controle exato de que parte do código está sendo executado em determinados momentos de um ciclo de vida de um script, além da alta quantidade de códigos em um único arquivo devido as repetições de códigos já declarado anteriormente. Porém a oop visa resolver todos estes problemas, isolando partes do código facilitando o debug durante o desenvolvimento de uma nova classe ou método do objeto, além do reaproveitamento de códigos sem precisar ficar repetindo um fluxo de código.
