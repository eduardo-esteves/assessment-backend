<?php
/**
 * Desenvolvido por Eduardo Esteves
 * 
 * (c) Eduardo Esteves <eduardostevesdasilva@hotmail.com>
 * 
 */

namespace app\lib;

use app\lib\Helpers;

/**
 * 1 kilobyte (kB ou Kbytes) = 1024 bytes
 * 1024*70 = 71680kb
 */
class SimpleUpload{

  private $files, $allowed, $maxSize, $error;
  public $uploaded;

  public function __construct($file,$allowed,$maxSize){
    $this->file = $file;
    $this->allowed = $allowed;
    $this->maxSize = $maxSize;
  }
  
  public function getErrors(){
    return $this->error;
  }

  public function validaFile(){
    
    //echo "<pre>"; print_r($this->file['image']); echo "</pre>";
    $file = $this->file['image'];
    // Caso nenhuma imagem for enviada não precisará de validação.
    if( empty($file['name']) and ($file['size'] === 0) ){
      return TRUE;
    }
      
    $newArray = explode("/", $file['type']);
    // Retorna o último elemento do array.
    $extension = end($newArray);

    if(!in_array($extension,$this->allowed)){
      $this->error[] = "Tipo de imagem inválida, você está tentando enviar um arquivo ".
      "com extensão $extension";
    }
    // Tanto $file[size] quanto $this->maxSize são do tipo int.
    if($file['size'] > $this->maxSize){
      $this->error[] = "O tamanho da imagem enviada é maior que o permitido ".
      "de $this->maxSize"."KB";
    }
    
    if( count($this->error) > 0 ){
      return FALSE;
    }

    return TRUE;
    
  }

  public function uploads($dir,$ID){
    /**
     * Diretório onde será salvo a imagem.
     * ../assets/uploads/83085462910a7c8a40b3ad64d0e01057e6614773
     * ../assets/uploads/8505af500c4905a2dae75afc1541b4c1853cff8a
     */
    $uploadDir = Helpers::sha1Salt($ID);   
    $fullDir = $dir.$uploadDir.'/'; 
    
    // Verifica se já existe o diretório onde se deseja salvar		
    if(!file_exists($fullDir) && !is_dir($fullDir)):
      if(	!mkdir($fullDir,0775,true) ):
          throw new \Exception('[Error]: Diretório informado ' .
          $fullDir . ' não existe e também não foi possível criar um novo diretório.');
      endif;
    endif;  
    
    $imagem = $this->file['image'];
    /**
     * Caso não haja imagem e nenhum erro, retorna a aplicação
     * setando a flag de controle uploaded.
     */
    if( empty($imagem['name']) and empty($imagem['size']) and empty($this->getErrors()) ){      
      return $this->uploaded = FALSE;
    }
    
    $ext = pathinfo($imagem['name'],PATHINFO_EXTENSION);
    // Nome do arquivo
    $fileName = time() . rand() . '.' . $ext;
    // Move a imagem para o diretório /uploads/idProduto/nome_do_arquivo.jpg
    $uploaded = move_uploaded_file($imagem['tmp_name'], $fullDir . $fileName);
    //echo "<pre>"; var_dump($uploaded); echo "</pre>";die;
    if(!$uploaded){
      throw new \Exception('[Error]: Não foi possível mover ' .
      'o arquivo ' . $fileName .' para a pasta '.
      $fullDir . ' verifique se a pasta existe ou tem as devidas '.
      'permissões de escrita no servidor.');
    }
    $this->uploaded = $uploadDir .'/'. $fileName;
    return TRUE;
  }
      

}