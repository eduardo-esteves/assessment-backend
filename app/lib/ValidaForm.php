<?php
/**
 * Desenvolvido por Eduardo Esteves
 * 
 * (c) Eduardo Esteves <eduardostevesdasilva@hotmail.com>
 * 
 */

namespace app\lib;

class ValidaForm{

  public static function validaCodigo($codigo){
    $pattern = '/^[a-z0-9]+$/i';
    if(!preg_match($pattern,$codigo)){
      return FALSE;
    }
    return TRUE;
  }

  public static function validaString($string){
    if(!is_string($string)){
      return FALSE;
    }
    return TRUE;
  }

  public static function validaMoeda($moeda){
    $pattern = '/^[0-9\.\,]+$/i';
    if(!preg_match($pattern,$moeda)){
      return FALSE;
    }
    return TRUE;
  }

  public static function validaInt($inteiro){
    if(!is_int($inteiro)){
      return FALSE;
    }
    return TRUE;
  }

}