<?
/**
 * Desenvolvido por Eduardo Esteves
 * 
 * (c) Eduardo Esteves <eduardostevesdasilva@hotmail.com>
 * 
 */

namespace app\lib;

class Helpers{
  /**
   * Retorna um identificador unico prefixado baseado no 
   * tempo atual em milionésimos de segundo.  
   */
  public static function genarateUniqId(){
    //http://php.net/manual/pt_BR/function.uniqid.php
    $uniqId = md5(uniqid(mt_rand(), true));
    return $uniqId;
  }
  // Reorganiza um array de uploads
  public static function reorgArray($arrayMult){
    foreach ($arrayMult as $key => $arrai) {
      foreach ($arrai as $interator => $valor) {
        $redes_sociais[$interator][$key] = $valor;
      }
    }
    return $redes_sociais;
  }
  // Limita uma string sem cortar-lá pela metade.
  public static function substrWords($text, $maxchar, $end='[...]') {
    if (strlen($text) > $maxchar || $text == '') {
      $words = preg_split('/\s/', $text);      
      $output = '';
      $i      = 0;
      while (1) {
          $length = strlen($output)+strlen($words[$i]);
          if ($length > $maxchar) {
            break;
          } 
          else {
            $output .= " " . $words[$i];
            ++$i;
          }
      }
      $output .= $end;
    } 
    else {
      $output = $text;
    }
    return $output;
  }
  // Gera um sha1+palavra secreta WebJump
  public static function sha1Salt($value){
    $sha1Salt = sha1($value.'WebJump'); 
    return $sha1Salt;
  }
  // Converte um imput com valor de moeda BR para USA
  public static function moneyToDB($money) {
    $source = array('.', ',');
    $replace = array('', '.');
    //remove os pontos e substitui a virgula pelo ponto
    $valor = str_replace($source, $replace, $money); 
    //retorna o valor formatado para gravar no banco
    return $valor; 
  }

}