<?php
/**
 * Desenvolvido por Eduardo Esteves
 * 
 * (c) Eduardo Esteves <eduardostevesdasilva@hotmail.com>
 * 
 */
namespace app\lib; 

class Banco {
	
	protected static $instancia;
	protected $conexao;
	private $_query;
	public $numRows;
	
	/**
	 * Aplico o design pattern singleton que garante
	 * que haverá apenas uma instância do objeto Banco
	 * executando na aplicação.
	 */
	public static function instanciar() {
		if(!self::$instancia) {
			self::$instancia = new Banco;
			self::$instancia->conectar();
		}
		
		return self::$instancia;
	}
	
	protected function conectar() {
		try{
			global $config;
			$this->conexao = new \PDO("{$config['driver']}:host={$config['host']};dbname={$config['database']}", $config['user'], $config['pass']);
			$this->conexao->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		}catch(\PDOException $e){
			echo "Falha na conexão com o banco de dados: " . $e->getMessage();
			return FALSE;
		}
	}

	public function statement(){
		return $this->_query;
	}

	public function executar($sql, $dados = null) {
		// preparo uma query a ser executada
		$this->_query = $this->conexao->prepare($sql);
		// executo a query
		try{
			// Capturo se a execução foi ou não bem sucedida e retorna um bool
			$statement = $this->_query->execute($dados); 
			$this->numRows = $this->_query->rowCount();						
			return $statement;
		}catch(\PDOException $e){
			$error = $e->getMessage() . 
      ' do arquivo '. $e->getFile() . 
      ' na linha ' . $e->getLine() .
      ' erro capturado no arquivo ' . __FILE__ . 
			' da linha ' . __LINE__ . '';
			//echo "<pre>"; print_r($error); echo "</pre>";die;
			return FALSE;
		}
	}

	public function inserir($tabela, $dados) {
		foreach($dados as $coluna => $valor) {
			$colunas[] = "`$coluna`";
			$substitutos[] = "?";
			$valores[] = $valor;
			$colVal []= '`'.$coluna.'` = ' . '\''.$valor.'\'';
		}

		$colunas = implode(", ", $colunas);
		$substitutos = implode(", ", $substitutos);
		$colVal = implode(', ', $colVal);

		$query = "INSERT INTO `$tabela` ($colunas) VALUES ($substitutos) ON DUPLICATE KEY UPDATE $colVal";
		
		//echo "<pre>"; print_r($query); echo "</pre>";die;
		return $this->executar($query, $valores);
	}

	public function remover($tabela, $field='id', $id) {
		$query = "DELETE FROM `$tabela`";

		if(!empty($id)) {
			$query .= " WHERE `$field` = ?";
		}
					
		return $this->executar($query, [$id]);
	}

}
