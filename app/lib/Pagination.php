<?
/**
 * Desenvolvido por Eduardo Esteves
 * 
 * (c) Eduardo Esteves <eduardostevesdasilva@hotmail.com>
 * 
 */

namespace app\lib;

class Pagination{

  protected $maxPerPage, $currentPage, $inicio, $numPages, $prev, $next;

  public function __construct($maxPerPage){
    $this->maxPerPage = $maxPerPage;
  }

  public function getMaxPerPage(){
    return $this->maxPerPage;
  }

  public function setCurrentPage($currentPage){
    $this->currentPage = $currentPage;
    return $this;
  }

  public function getCurrentPage(){
    return $this->currentPage;
  }

  public function pageInit(){
    //variavel para calcular o início da visualização com base na página atual 
    return $this->inicio = ( $this->maxPerPage * (int)$this->currentPage ) - $this->maxPerPage;
  }
  // Número de páginas retornada pela consulta
  public function setNumPages($numPages){
    $this->numPages = ceil($numPages/$this->maxPerPage);
    return $this;
  }
  // Retorna o total de paginas da consulta SQL atual
  public function getNumPages(){
    return $this->numPages;
  }
  // Retorna a página anterior
  public function getPrev(){
    return $this->prev = (($this->currentPage - 1) == 0) ? '1' : $this->currentPage - 1;
  }
  // Retorna a página posterior eduardo nicolas marilia
  public function getNext(){
    return $this->next = (($this->currentPage + 1) > $this->numPages) 
    ? $this->numPages 
    : $this->currentPage + 1;
  }

}