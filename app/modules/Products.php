<?php

namespace app\modules;

use app\lib\Pagination;

class Products{

  public function __construct(\app\lib\Banco $database){
    $this->database = $database->instanciar();
  }

  public function index($get){
    $currentPage = (!empty($get['page'])) ? $get['page'] : 1;
    $Pagination = new Pagination(2);
    $Pagination->setCurrentPage($currentPage);
    
    $sql = "SELECT `id`,`produto`,`preco`,`quantidade`,`imagem` 
    FROM `produtos` LIMIT " . $Pagination->pageInit() . 
    ", " . $Pagination->getMaxPerPage() . " ";
    $stmt = $this->database->executar($sql);
    if($stmt){
      $data['produtos'] = $this->database->statement()->fetchAll(\PDO::FETCH_ASSOC);
      //Total de paginas a serem exibidas, 2º parte da paginação
      $sql = "SELECT `id` FROM `produtos`";
      $stmt = $this->database->executar($sql);
      $Pagination->setNumPages($this->database->numRows);
      $data['pagination'] = $Pagination;
      //echo "<pre>"; print_r($numPage); echo "</pre>";
    }
    return $data;
  }

  public function view($get){
    $id = $get['id'];
    //echo "<pre>"; print_r($id); echo "</pre>";
    // Preparo minha consulta
    $sql = "SELECT * FROM `produtos` WHERE `id` = ?";
    // A executo passando o segundo parâmetro, para ser feito o bind.
    $stmt = $this->database->executar($sql,[$id]);
    // Caso não retorne erro
    if($stmt){
      $data['produto'] = $this->database->statement()->fetch(\PDO::FETCH_ASSOC);
    }
    $sql = "SELECT  `c`.`*`
    FROM `categorias` as `c`
    LEFT JOIN `produtos_categorias` as `pc`
    ON `pc`.`categoria_id` = `c`.`id`
    LEFT JOIN `produtos` as `p`
    ON `pc`.`produto_id` = `p`.`id`
    WHERE `p`.`id` = ?";
    $stmt = $this->database->executar($sql,[$id]);
    if($stmt){
      $data['categorias'] = $this->database->statement()->fetchAll(\PDO::FETCH_ASSOC);
    }
    return $data;
  }

}