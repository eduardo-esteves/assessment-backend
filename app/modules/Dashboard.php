<?php

namespace app\modules;

use app\lib\Helpers;
use app\lib\SimpleUpload;
use app\lib\ValidaForm;
use app\lib\Pagination;

class Dashboard{

  public function __construct(\app\lib\Banco $database){
    $this->database = $database->instanciar();
  }

  // Action principal de apresentação dos dados
  public function index(){ 
    $sql = "SELECT `id`,`codigo`,`produto`,`preco`,`quantidade` 
    FROM `produtos` ";   
    $stmt = $this->database->executar($sql);
    if($stmt){
      $data['produto'] = $this->database->statement()->fetchAll(\PDO::FETCH_ASSOC);
    }
    return $data;
  }

  public function products($get){ 
    $currentPage = (!empty($get['page'])) ? $get['page'] : 1;
    $Pagination = new Pagination(2);
    $Pagination->setCurrentPage($currentPage);

    $sql = "SELECT `id`,`codigo`,`produto`,`preco`,`quantidade` 
    FROM `produtos` LIMIT " . $Pagination->pageInit() . 
    ", " . $Pagination->getMaxPerPage() . " ";
        
    $stmt = $this->database->executar($sql);

    if($stmt){
      $data['produto'] = $this->database->statement()->fetchAll(\PDO::FETCH_ASSOC);
      //Total de paginas a serem exibidas, 2º parte da paginação
      $sql = "SELECT `id` FROM `produtos`";
      $stmt = $this->database->executar($sql);
      $numRows = $this->database->numRows;
      $Pagination->setNumPages($numRows);
      $data['total'] = $numRows;
      $data['pagination'] = $Pagination;
    }
    return $data;
  }

  public function product_create(){
        
    if(!empty($_POST['product']['save_product'])){

      $product = $_POST['product'];
      $categories = (!empty($product['categorias'])) ? $product['categorias'] : '';
      $imagem = Helpers::reorgArray($_FILES['product']);
      if( !empty($product['img_src']) and !empty($product['id']) ){
        $imgSrc = "./assets/uploads/{$product['img_src']}";
        unset($product['img_src']);
      } 

      $SimpleUpload = new SimpleUpload($imagem,['jpg','jpeg','png'],71680);
      $validaFile = $SimpleUpload->validaFile();
            
      $data['error'] = FALSE;

      if(!$validaFile){
        $errorMsg = $SimpleUpload->getErrors();
        foreach ($errorMsg as $msg) {
          $error[] = $msg;
        }
        
      }
  
      $product['preco'] = Helpers::moneyToDB($product['preco']);
      $product['descricao'] = htmlentities(str_replace('\'','',$product['descricao']),ENT_NOQUOTES,"UTF-8");
      //echo "<pre>"; print_r($product); echo "</pre>";die;
      if( empty($product['codigo']) or !ValidaForm::validaCodigo($product['codigo']) ){
        $error[] = 'Código SKU '.$product['codigo'].'inválido tente somente letras e números sem espaços.';
      } 
      if( empty($product['produto']) or !ValidaForm::validaString($product['produto']) ){
        $error[] = 'Nome do produto '.$product['produto'].' inválido tente somente letras e números.';
      } 
      if( empty($product['preco']) or !ValidaForm::validaMoeda($product['preco']) ){
        $error[] = 'Preço informado '.$product['preco'].' inválido tente somente números.';
      }
      if( empty($product['quantidade']) or !ValidaForm::validaInt((int)$product['quantidade']) ){
        $error[] = 'Quantidade informada '.$product['quantidade'].' inválida tente somente números.';
      }
      if( empty($product['categorias']) ){
        $error[] = 'Categoria ausente, escolha ao menos uma categoria.';
      }
      
      //echo "<pre>"; print_r($error); echo "</pre>";die;
      
      if( !empty($error) ){
        $data['error'] = $error;
        return $data;
      }
            
      /**
       * Caso passe pela validação dos dados prossegue 
       * apenas com a inserção do produto.
       */
      //echo "<pre>"; print_r($product); echo "</pre>";
      //echo "<pre>"; print_r($imgSrc); echo "</pre>";die;
      $product['id'] = (empty($product['id'])) 
        ? Helpers::genarateUniqId()
        : $product['id'];
      unset($product['save_product'],$product['categorias']);
      $savedProduct = $this->database->inserir('produtos',$product); 
      //echo "<pre>"; var_dump($savedProduct); echo "</pre>";die;
      $data['saved'] = FALSE;
      /**
       * Caso for salvo o produto e as categorias, 
       * sobreescre o valor de $data['saved'] para TRUE  
       * e retorna imediatamente ao controller. e caso haja algum erro 
       * tanto para salvar o produto quanto as categorias, permanecerá
       * $data['saved'] como FALSE e devolverá este valor de imediato ao controller.
       */
      if($savedProduct){ 
        // Caso se trate de um update deleto as categorias para este produto.
        if(!empty($product['id'])){
          $catDelete = $this->database->remover('produtos_categorias', 'produto_id', $product['id']);
          if(!$catDelete){ 
            return $data;
          }
        }
        // Insiro as novas categorias selecionadas.
        foreach ($categories as $category) {
          $savedCategories = $this->database->inserir('produtos_categorias',[
            'id' => Helpers::genarateUniqId(),
            'produto_id' => $product['id'],
            'categoria_id' => $category,
          ]);
                   
          if(!$savedCategories) return $data;
        }
        /**
         * Caso o produto e as categorias foram salva com sucesso tenta
         * fazer o upload. Se nenhuma exceção for lançada por uploads()
         * seta a flag de controle save para true e retorna para o controller.
         * caso contrário $data['saved'] permanece FALSE.
         */
        try{
          $SimpleUpload->uploads('./assets/uploads/',$product['id']);
          //echo "<pre>"; var_dump($SimpleUpload); echo "</pre>";die;
          if( !empty($SimpleUpload->uploaded) and empty($SimpleUpload->getErrors()) ){
            $sql = $sql = "UPDATE `produtos` SET `imagem` = ? WHERE `id` = ? ";
            $this->database->executar($sql,[$SimpleUpload->uploaded, $product['id']]);
            if(!empty($imgSrc)){
             unlink($imgSrc);
            } 
            $data['saved'] = TRUE; 
            // Caso nenhum arquivo foi enviado, não faz nenhum upload.
          }elseif( empty($SimpleUpload->uploaded) and empty($SimpleUpload->getErrors()) ){
            $data['saved'] = TRUE;
          }
        }catch(\Exception $e){
          $error = $e->getMessage() . 
          ' do arquivo '. $e->getFile() . 
          ' na linha ' . $e->getLine() .
          ' erro capturado no arquivo ' . __FILE__ . 
          ' da linha ' . __LINE__ . '';
          echo "<pre>"; print_r($error); echo "</pre>";die;
          return $data;
        }
        
      }
      return $data;
    }
    
    $sql = "SELECT * FROM `categorias`";
    $stmt = $this->database->executar($sql);
    
    if($stmt){
      $data['categorias'] = $this->database->statement()->fetchAll(\PDO::FETCH_ASSOC);
    }

    return $data;
  }

  public function product_edit($get){
    $id = $get['id'];
    //echo "<pre>"; print_r($get); echo "</pre>";die;
    if(ValidaForm::validaString($id) and strlen($id) > 30){
      
      // Preparo minha consulta
      $sql = "SELECT * FROM `produtos` WHERE `id` = ?";
      // A executo passando o segundo parâmetro, para ser feito o bind.
      $stmt = $this->database->executar($sql,[$id]);
      
      if($stmt){
        $data['produto'] = $this->database->statement()->fetch(\PDO::FETCH_ASSOC);
      }else{
        $data['error'] = ['Dados não encontrado'];
        return $data;
      }
      
      $sql = "SELECT * FROM `categorias`";
      $stmt = $this->database->executar($sql);
      
      if($stmt){
        $data['categorias'] = $this->database->statement()->fetchAll(\PDO::FETCH_ASSOC);
      }else{
        $data['error'] = ['Dados não encontrado'];
        return $data;
      }
      
      $sql = "SELECT * FROM `produtos_categorias` WHERE `produto_id` = ?";   
      $stmt = $this->database->executar($sql,[$id]); 

      if($stmt){
        $data['categoriasProdutos'] = $this->database->statement()->fetchAll(\PDO::FETCH_ASSOC);
      }else{
        $data['error'] = ['Dados não encontrado'];
        return $data;
      }
      
    }else{
      $data['error'] = ['Produto informado não encontrado!'];
    }
    return $data;
  }

  public function product_delete($get){
    $id = $get['id'];
    //echo "<pre>"; print_r($id); echo "</pre>";die;
    
    // Evita usuários ficar testando
    if(ValidaForm::validaString($id) and strlen($id) > 30){
      $sql = "DELETE FROM `produtos` WHERE `id` = ?";
      $stmt = $this->database->executar($sql,[$id]);
            
      if($stmt){
        $data['saved'] = TRUE;
      }else{
        $data['saved'] = FALSE;
      }
      return $data;
    }else{
      $data['error'] = ['Categoria informada não encontrada!'];
      return $data;
    }
    return $data;
  }

  public function categories($get){
    $currentPage = (!empty($get['page'])) ? $get['page'] : 1;
    $Pagination = new Pagination(2);
    $Pagination->setCurrentPage($currentPage);

    $sql = "SELECT `*` FROM `categorias` LIMIT " . $Pagination->pageInit() . 
    ", " . $Pagination->getMaxPerPage() . " ";
    $stmt = $this->database->executar($sql);

    if($stmt){
      $data['categoria'] = $this->database->statement()->fetchAll(\PDO::FETCH_ASSOC);
      //Total de paginas a serem exibidas, 2º parte da paginação
      $sql = "SELECT `id` FROM `categorias`";
      $stmt = $this->database->executar($sql);
      $numRows = $this->database->numRows;
      $Pagination->setNumPages($numRows);
      $data['pagination'] = $Pagination;
    }else{
      $data['error'] = ['Não há categoria cadastrada!'];
      return $data;
    }
    return $data;
  }

  public function category_create(){

    if(!empty($_POST['category']['save_category'])){
      
      $category = $_POST['category'];
      
      if( empty($category['codigo']) ){
        $error[] = 'Código '.$category['codigo'].'inválido tente somente letras e números sem espaços.';
      } 
      if( empty($category['categoria']) or !ValidaForm::validaString($category['categoria']) ){
        $error[] = 'Nome do categoria '.$category['categoria'].' inválido tente somente letras e números.';
      } 
      
      if( !empty($error) ){
        $data['error'] = $error;
        return $data;
      }
      
      unset($category['save_category']);
      $category['id'] = (empty($category['id'])) 
        ? Helpers::genarateUniqId()
        : $category['id'];
      //echo "<pre>"; print_r($category); echo "</pre>";die;
      
      $savedCategory = $this->database->inserir('categorias',$category); 
      
      $data['saved'] = FALSE;
            
      if($savedCategory){
        $data['saved'] = TRUE;
      }
      
      return $data;

    }
    //return ['categoria' => 'criar'];
  }

  public function category_edit($get){
    $id = $get['id'];
    // Evita usuários ficar testando
    if(ValidaForm::validaString($id) and strlen($id) > 30){
      $sql = "SELECT * FROM `categorias` WHERE `id` = ?";
      // A executo passando o segundo parâmetro, para ser feito o bind.
      $stmt = $this->database->executar($sql,[$id]);

      if($stmt){
        $data['categoria'] = $this->database->statement()->fetch(\PDO::FETCH_ASSOC);
      }else{
        $data['error'] = ['Categoria informada não encontrada!'];
        return $data;
      }
    }else{
      $data['error'] = ['Categoria informada não encontrada!'];
    }
    return $data;
  }

  public function category_delete($get){
    $id = $get['id'];
    // Evita usuários ficar testando
    if(ValidaForm::validaString($id) and strlen($id) > 30){
      $sql = "DELETE FROM `categorias` WHERE `id` = ?";
      $stmt = $this->database->executar($sql,[$id]);
      if($stmt){
        $data['saved'] = TRUE;
      }else{
        $data['saved'] = FALSE;
      }      
      return $data;
    }else{
      $data['error'] = ['Categoria informada não encontrada!'];
    }
    return $data;
  }
  
}
