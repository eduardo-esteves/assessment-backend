<?php

namespace app\modules;

use League\Csv\Writer;

class Csv{

  public function __construct(\app\lib\Banco $database){
    $this->database = $database->instanciar();
  }

  // https://github.com/thephpleague/csv/blob/master/docs/9.0/index.md
  public function example(){
    $header = ['first name', 'last name', 'email'];
    $records = [
      [1, 2, 3],
      ['foo', 'bar', 'baz'],
      ['john', 'doe', 'john.doe@example.com'],
    ];
    
    //we create the CSV into memory
    $csv = Writer::createFromFileObject(new \SplTempFileObject());
    $csv->setDelimiter(';');
    //insert the header
    $csv->insertOne($header);
    //insert all the records
    $csv->insertAll($records);
    //returns the CSV document
    $csv->output('users.csv');
    die(); 
  }

  public function export_to_products($get){
    $table = $get['table'];
        
    $sql = "SELECT `produto`, `codigo`, `descricao`, 
    `quantidade`, `preco`
    FROM `$table`";
    $stmt = $this->database->executar($sql);
    if($stmt){
      $produtos = $this->database->statement()->fetchAll(\PDO::FETCH_ASSOC);
      $header = ['Nome', 'SKU', 'Descricao', 'Quantidade', 'Preco'];
      //we create the CSV into memory
      $csv = Writer::createFromFileObject(new \SplTempFileObject());
      $csv->setDelimiter(';');
      //insert the header
      $csv->insertOne($header);
      //insert all the records
      $csv->insertAll($produtos);
      //returns the CSV document
      $csv->output('products.csv');
    }
    die(); 
  }

  public function export_to_categories($get){
    $table = $get['table'];
        
    $sql = "SELECT `codigo`, `categoria` FROM `$table`";
    $stmt = $this->database->executar($sql);
    if($stmt){
      $produtos = $this->database->statement()->fetchAll(\PDO::FETCH_ASSOC);
      $header = ['Codigo', 'Categoria'];
      //we create the CSV into memory
      $csv = Writer::createFromFileObject(new \SplTempFileObject());
      $csv->setDelimiter(';');
      //insert the header
      $csv->insertOne($header);
      //insert all the records
      $csv->insertAll($produtos);
      //returns the CSV document
      $csv->output('categories.csv');
    }
    die(); 
  }

  
}
