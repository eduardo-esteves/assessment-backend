<?php

namespace app\core;

use app\core\View;
use app\modules\Products;
use app\lib\Banco;
use app\modules\Dashboard;
use app\modules\Csv;

class Controller {

	protected $view;
	protected $module;
	protected $action;
	protected $products;
	protected $dashboard;
	protected $csv;
	
	public function __construct() {

		$this->view = new View;
		$this->products = new Products(Banco::instanciar());
		$this->dashboard = new Dashboard(Banco::instanciar());
		$this->csv = new Csv(Banco::instanciar());
				
		$data = $this->loadModules();
		//echo "<pre>"; print_r($data); echo "</pre>";die;
		/**
		 * Carrega a view conforme os parametros recebidos via $_GET
		 * ex: $this->view->load("products/listar", $data);
		 */
		if(isset($data['saved']) and $data['saved'] === TRUE){
			header('Location: ?modules='.$this->module.'&action=index');
		}elseif(isset($data['saved']) and $data['saved'] === FALSE) {
			$this->view->load("error/fatal", $data = [
				'msg' => 'Houve um erro inesperado ao processar essa requisição, ' .
				'por favor contactar o administrador do sistema.'
			]);
		}elseif(isset($data['error']) and $data['error'] !== FALSE){
			$this->view->load("error/error-msg", $data = [
				'title' => 'Erro ao validar os dados',
				'error' => $data['error'],
			]);
		}else{
			$this->view->load("$this->module/$this->action", $data);
		}
		
	}
	// Método responsável por carregar os dados a ser utilizado na View
	protected function loadModules(){
		$module = ( !empty($_GET['module']) ? $_GET['module'] : 'products');
		$this->module = $module;
		$action = ( !empty($_GET['action']) ? $_GET['action'] : 'index');
		$this->action = $action;
		/**
		 * $this->$module vai carregar a classe e action conforme 
		 * os parametros recebido via $_GET ex: $this->products->listar()
		 */
		if( isset($_GET) ){				
			$data = $this->$module->$action($_GET);
		}else{
			$data = $this->$module->$action(); 
		}
		return $data;
	}
	
}
