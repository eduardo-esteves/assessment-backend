<?php

namespace app\core;

class View {
	public function load($file, $data = null) {
		include("./app/views/header.tpl.php");
		include("./app/views/$file.tpl.php");
		include("./app/views/footer.tpl.php");
	}
}
