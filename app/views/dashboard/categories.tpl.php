<?
//echo "<pre>"; print_r($data); echo "</pre>";
$Pagination = $data['pagination'];
?>

<div class="header-list-page">
  <h1 class="title">Categories</h1>
  <a href="index.php?module=dashboard&action=category_create" class="btn success">Add new Category</a>
</div>
<div class="import-csv" style="float:left;display:inline-block;">
  <button 
    class="back btn"
    id="exportToCSV"
    data-table="categorias"
    data-url="index.php?module=csv&amp;action=export_to_categories">
      Import to .csv
  </button>
  </div>
<table class="data-grid">
  <tr class="data-row">
    <th class="data-grid-th">
        <span class="data-grid-cell-content">Name</span>
    </th>
    <th class="data-grid-th">
        <span class="data-grid-cell-content">Code</span>
    </th>
    <th class="data-grid-th">
        <span class="data-grid-cell-content">Actions</span>
    </th>
  
    <? 
    foreach ($data['categoria'] as $categoria) { ?>
    </tr>
      <td class="data-grid-td">
        <span class="data-grid-cell-content"><?=$categoria['categoria']?></span>
      </td>
      <td class="data-grid-td">
        <span class="data-grid-cell-content"><?=$categoria['codigo']?></span>
      </td>
      <td class="data-grid-td">
        <div class="actions">
          <a href="index.php?module=dashboard&action=category_edit&id=<?=$categoria['id']?>" 
              class="warning btn edit" 
              data-name="<?=$categoria['categoria']?>" 
              data-id="<?=$categoria['id']?>">
              <span>Edit</span>
          </a>
          <a href="index.php?module=dashboard&action=category_delete&id=<?=$categoria['id']?>" 
            class="danger btn delete" 
            data-name="<?=$categoria['categoria']?>" 
            data-id="<?=$categoria['id']?>">
            <span>Delete</span>
          </a>
        </div>
      </td> 
      </tr><?
    }
    ?>
</table>

<div class="pagination">
  <nav class="pag-itens">
    <a href=<?= ($Pagination->getPrev() == $Pagination->getCurrentPage()) ? "#" : 'index.php?module=dashboard&action=categories&page='.$Pagination->getPrev().''?>>
        &laquo;
    </a>
      <? for($i = 1; $i <= $Pagination->getNumPages(); $i++){?>
        <li>
          <a 
            href="index.php?module=dashboard&action=categories&page=<?=$i?>"
            class="<?= ($i == $Pagination->getCurrentPage()) ? 'active' : '' ?>" >
            <?=$i?>
          </a>
        </li> <?
          
      }?>
    <a href="index.php?module=dashboard&action=categories&page=<?=$Pagination->getNext()?>">&raquo;</a>
  </nav>
</div>