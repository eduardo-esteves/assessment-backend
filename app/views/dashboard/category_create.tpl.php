<?echo "<pre>"; print_r($data); echo "</pre>";
?>
<form method="POST" action="?module=dashboard&action=category_create">
  <div class="input-field">
    <label for="category-name" class="label">
      Category Name <span style="color:red;">*</span>
    </label>
    <input 
      type="text" 
      name="category[categoria]" 
      id="category-name" 
      class="input-text" 
      maxlength="100"
      title="Input Category Name invalid."
      required
    />
    
  </div>
  <div class="input-field">
    <label for="category-code" class="label">
      Category Code <span style="color:red;">*</span>
    </label>
    <input 
      type="text"
      name="category[codigo]" 
      id="category-code" 
      class="input-text" 
      pattern="^[a-z0-9]+$"
      maxlength="100"
      title="Input Category Code invalid, try patter 54df5d21f for example."
      required
    />
    
  </div>
  <div class="actions-form">
    <a href="index.php?module=dashboard&action=categories" class="back btn">Back</a>
    <input type="submit" name="category[save_category]" class="btn-submit success btn" value="Save">
  </div>
</form>