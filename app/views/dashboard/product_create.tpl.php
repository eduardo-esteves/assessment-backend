<?php
  //echo "<pre>"; print_r($data); echo "</pre>";  
?>
  
  <h1 class="title new-item">New Product</h1>
  
  <form method="POST" action="?module=dashboard&action=product_create" enctype="multipart/form-data">
    <div class="product-image" style="text-align:center">
      <img src="assets/images/add-img.png" width="300" height="250" id="imgPreview">
    </div>
    <div class="input-field">
      <label for="image" class="label">Product Image</label>
      <input type="file" name="product[image]" id="image" class="input-text" onchange="previewImage(event)" /> 
    </div>
    <div class="input-field">
      <label for="codigo" class="label">
        Product SKU <span style="color:red;">*</span>
      </label> 
      <input 
        type="text" 
        name="product[codigo]" 
        id="codigo" 
        class="input-text" 
        pattern="^[a-z0-9]+$"
        maxlength="100"
        title="Input Product SKU invalid, try patter 54df5d21f for example."
        required
      /> 
    </div>
    <div class="input-field">
      <label for="name" class="label">
        Product Name <span style="color:red;">*</span>
      </label>
      <input 
        type="text" 
        name="product[produto]" 
        id="name" 
        class="input-text" 
        maxlength="100"
        required
      /> 
    </div>
    <div class="input-field">
      <label for="price" class="label">
        Price <span style="color:red;">*</span>
      </label>
      <input 
        type="text" 
        name="product[preco]" 
        id="price" 
        class="input-text"
        maxlength="15" 
        pattern="^[0-9\.\,]+$"
        title="Input Price invalid, try pattern 20,00 for example."
        required
      /> 
    </div>
    <div class="input-field">
      <label for="quantity" class="label">
        Quantity <span style="color:red;">*</span>
      </label>
      <input 
        type="number" 
        name="product[quantidade]" 
        id="quantity" 
        class="input-text" 
        maxlength="11"
        pattern="^[0-9\.,]+$"
        title="Input quantity invalid, try pattern 10 for example."
        required
      /> 
    </div>
    <div class="input-field">
      <label for="category" class="label">
        Categories <span style="color:red;">*</span>
      </label>
      <select 
        name="product[categorias][]" 
        multiple id="category" 
        class="input-text"
        required>
        <? foreach ($data['categorias'] as $data) { ?>
          <option value="<?=$data['id']?>"><?=$data['categoria']?></option> <?
        } ?>
      </select>
    </div>
    <div class="input-field">
      <label for="description" class="label">
        Description <span style="color:red;">*</span>
      </label>
      <textarea name="product[descricao]" id="description" class="input-text"></textarea>
    </div>
    <div class="actions-form">
      <a href="index.php?module=dashboard&action=products" class="back btn">Back</a>
      <input type="submit" name="product[save_product]" class="btn-submit success btn" value="Save Product" />
    </div>
  </form>