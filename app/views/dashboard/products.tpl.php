
  <?
    //echo "<pre>"; print_r($data); echo "</pre>";
    use app\lib\Helpers;
    
    $Pagination = $data['pagination'];
  ?>  

  <div class="header-list-page">
    <h1 class="title">Dashboard</h1>
    <a href="?module=dashboard&action=product_create" class="success btn">Add new Product</a>
  </div>
  <div class="import-csv" style="float:left;display:inline-block;">
    <button 
      class="back btn"
      id="exportToCSV"
      data-table="produtos"
      data-url="index.php?module=csv&amp;action=export_to_products">
        Import to .csv
    </button>
  </div>
  <div class="infor" style="float:right;display:inline-block;">
    <span>You have <?=$data['total']?> products added on this store.</span>
  </div>
<table class="data-grid">
  <tr class="data-row">
    <th class="data-grid-th">
      <span class="data-grid-cell-content">Name</span>
    </th>
    <th class="data-grid-th">
      <span class="data-grid-cell-content">SKU</span>
    </th>
    <th class="data-grid-th">
      <span class="data-grid-cell-content">Price</span>
    </th>
    <th class="data-grid-th">
      <span class="data-grid-cell-content">Quantity</span>
    </th>
    <th class="data-grid-th">
      <span class="data-grid-cell-content">Categories</span>
    </th>

    <th class="data-grid-th">
      <span class="data-grid-cell-content">Actions</span>
    </th>
  </tr>
  
  <? 
  foreach ($data['produto'] as $produto){ ?>
    <tr class="data-row">
      <td class="data-grid-td">
          <span class="data-grid-cell-content"><?=Helpers::substrWords($produto['produto'], 25, "...")?></span>
      </td>
      <td class="data-grid-td">
          <span class="data-grid-cell-content"><?=$produto['codigo']?></span>
      </td> 
      <td class="data-grid-td">
          <span class="data-grid-cell-content">R$ <?=number_format($produto['preco'], 2, ",", ".")?></span>
      </td> 
      <td class="data-grid-td">
          <span class="data-grid-cell-content"><?=$produto['quantidade']?></span>
      </td>  
      <td class="data-grid-td">
          <span class="data-grid-cell-content">Categoria</span>
      </td>
      <td class="data-grid-td">
        <div class="actions">
          <a href="index.php?module=dashboard&action=product_edit&id=<?=$produto['id']?>" 
            class="warning btn edit" 
            data-name="<?=$produto['produto']?>" 
            data-id="<?=$produto['id']?>">
            <span>Edit</span>
          </a>
          <a href="index.php?module=dashboard&action=product_delete&id=<?=$produto['id']?>" 
            class="danger btn delete" 
            data-name="<?=$produto['produto']?>" 
            data-id="<?=$produto['id']?>">
            <span>Delete</span>
          </a>
        </div>
      </td> 
    </tr> <?
  } ?>
  
</table>

<div class="pagination">
  <nav class="pag-itens">
    <a href=<?= ($Pagination->getPrev() == $Pagination->getCurrentPage()) ? "#" : 'index.php?module=dashboard&action=products&page='.$Pagination->getPrev().''?>>
        &laquo;
    </a>
      <? for($i = 1; $i <= $Pagination->getNumPages(); $i++){?>
        <li>
          <a 
            href="index.php?module=dashboard&action=products&page=<?=$i?>"
            class="<?= ($i == $Pagination->getCurrentPage()) ? 'active' : '' ?>" >
            <?=$i?>
          </a>
        </li> <?
          
      }?>
    <a href="index.php?module=dashboard&action=products&page=<?=$Pagination->getNext()?>">&raquo;</a>
  </nav>
</div>
  