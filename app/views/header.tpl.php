<!doctype html>
<html ⚡>
<head>
  <title>Webjump | Backend Test | Products</title>
  <meta charset="utf-8">

<link  rel="stylesheet" type="text/css"  media="all" href="assets/css/style.css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
<meta name="viewport" content="width=device-width,minimum-scale=1">
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
</head>
<!-- Header -->
<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
  <div class="close-menu">
    <a on="tap:sidebar.toggle">
      <img src="assets/images/bt-close.png" alt="Close Menu" width="24" height="24" />
    </a>
  </div>
  <a href="dashboard.html"><img src="assets/images/menu-go-jumpers.png" alt="Welcome" width="200" height="43" /></a>
  <div>
    <? 
      if( !empty($_GET['module']) and $_GET['module'] == 'dashboard' ){ ?>
        <ul>
          <li><a href="index.php?module=dashboard&action=categories" class="link-menu">Categorias</a></li>
          <li><a href="index.php?module=dashboard&action=products" class="link-menu">Produtos</a></li>
        </ul> <?
      }else{?>
        <ul>
          <li><a href="index.php?module=products" class="link-menu">Produtos</a></li>
        </ul> <?
      }
    ?>
    
  </div>
</amp-sidebar>
<header>
  <div class="go-menu">
    <a on="tap:sidebar.toggle">☰</a>
    <a href="dashboard.html" class="link-logo"><img src="assets/images/go-logo.png" alt="Welcome" width="69" height="430" /></a>
  </div>
  <div class="right-box">
    <? 
    if( !empty($_GET['module']) and $_GET['module'] != 'dashboard' ){ ?>
      <a href="index.php?module=dashboard&action=products"><span class="go-title">Administration Panel</span></a> <?
    }else{ ?>
      <a href="index.php?module=products&action=index"><span class="go-title">Back to Shop</span></a> <?
    }
    ?>
  </div>    
</header>  
<!-- Header -->
<body>
<!-- Main Content -->
<main class="content">
