<?php
    //echo "<pre>"; print_r($data); echo "</pre>";
    use app\lib\Helpers;

    $Pagination = $data['pagination']
?>

<div class="header-list-page">
    <h1 class="title">Products</h1>
</div>
<ul class="product-list">
<? foreach($data['produtos'] as $key => $produto){ ?>
    <li>
        <div class="product-image">
            <a href="index.php?module=products&action=view&id=<?=$produto['id']?>">
                <img 
                    src="assets/uploads/<?=$produto['imagem']?>" 
                    layout="responsive" width="164" height="145" 
                    alt="<?=Helpers::substrWords($produto['produto'], 60, "...");?>" 
                />
            </a>
        </div>
        <div class="product-info">
            <div class="product-name">
                <span>
                    <?=Helpers::substrWords($produto['produto'], 25, '<a href="index.php?module=products&action=view&id='.$produto['id'].'"> [...]</a>');?>
                </span>
            </div>
            <div class="product-price">
                <span class="special-price"><?=$produto['quantidade']?> available</span> 
                <span>R$<?=number_format($produto['preco'], 2, ",", ".")?></span>
            </div>
        </div>
    </li> <?
} ?>
</ul>

<div class="pagination">
    <nav class="pag-itens">
        <a href=<?= ($Pagination->getPrev() == $Pagination->getCurrentPage()) ? "#" : 'index.php?module=products&action=index&page='.$Pagination->getPrev().''?>>
            &laquo;
        </a>
        <? for($i = 1; $i <= $Pagination->getNumPages(); $i++){?>
            <li>
                <a 
                    href="index.php?module=products&action=index&page=<?=$i?>"
                    class="<?= ($i == $Pagination->getCurrentPage()) ? 'active' : '' ?>" >
                    <?=$i?>
                </a>
            </li> <?
            
        }?>
        <a href="index.php?module=products&action=index&page=<?=$Pagination->getNext()?>">&raquo;</a>
    </nav>
</div>