<?php
  //echo "<pre>"; print_r($data); echo "</pre>";
  use app\lib\Helpers; 
  $produto = $data['produto'];
  $categorias = $data['categorias'];
?>

<div class="header-list-page">
  <h1 class="title"><?=$produto['produto']?></h1>
</div>

<div class="product-view">
  <a href="index.php?module=products&action=view&id=<?=$produto['id']?>">
    <img 
      src="assets/uploads/<?=$produto['imagem']?>" 
      layout="responsive" width="500px" height="auto" 
      alt="<?=Helpers::substrWords($produto['produto'], 60, "...");?>"
      style="margin:auto;" 
    />
  </a>
  <div class="product-view cod">
    <span style="color:#87d;float:left;">Cód.<?=$produto['codigo']?></span>
    <span style="color:#87d;float:right;">R$<?=number_format($produto['preco'], 2, ",", ".")?></span>
  </div>
</div>
<div class="product-view info">
  <?=$produto['descricao']?>
  <div class="tag-cats" style="margin:30px 0">
    <? foreach ($categorias as $categoria) { ?>
      <span class="tags"><?=$categoria['categoria']?></span> <?
    } ?>
  </div>
</div>
